package com.example.kittyhub.ui.favouriteList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kittyhub.R
import com.example.kittyhub.features.AdapterKittyList
import com.example.kittyhub.features.CovertConfig
import kotlinx.android.synthetic.main.fragment_favourite_list.view.*
import nz.co.trademe.covert.Covert
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class FavouriteListFragment : Fragment(), IFavouriteListFragment {

    private val presenter: IFavouriteListPresenter by inject { parametersOf(this@FavouriteListFragment) }
    private lateinit var recyclerView: RecyclerView
    private val adapter: AdapterKittyList by inject()
    private val covertConfig: CovertConfig by inject()
    private lateinit var covert: Covert

    fun newInstance(): FavouriteListFragment {
        val regFragment = FavouriteListFragment()
        val args = Bundle()
        regFragment.arguments = args
        return regFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favourite_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.favouriteToolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        view.favouriteToolbar.setNavigationOnClickListener { activity!!.onBackPressed() }
        presenter.start()
    }

    override fun init() {
        recyclerView = view!!.findViewById(R.id.recyclerFavouriteViewKitty)
        recyclerView.layoutManager = LinearLayoutManager(context)
        presenter.openFavouriteKitty()
    }

    override fun showError(message: String) {
        recyclerView.adapter = adapter
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun loadDataInList(photoUrl: List<String>) {
        adapter.photoList = photoUrl.toMutableList()
        covert = Covert.with(covertConfig.create())
                .setIsActiveCallback { viewHolder -> presenter.isActive(adapter.getUrlByPosition(viewHolder.adapterPosition)) }
                .doOnSwipe { viewHolder, _ ->  presenter.changeFavouriteState(adapter.getUrlByPosition(viewHolder.adapterPosition)) }
                .attachTo(recyclerView)
        adapter.covert = covert
        recyclerView.adapter = adapter
    }

}
