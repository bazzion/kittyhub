package com.example.kittyhub.ui.kittyList

import android.annotation.SuppressLint
import com.example.kittyhub.useCase.IKittyDataUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class KittyListPresenter(private val view: IKittyListFragment, private val model: IKittyDataUseCase) : IKittyListPresenter {

    override fun start() {
        view.init()
    }

    @SuppressLint("CheckResult")
    override fun loadKitty() {
        model.getRemoteData().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            view.loadDataInList(it)
                            view.setRefreshState(false)
                        },
                        { t ->
                            view.showError(t.localizedMessage)
                            view.setRefreshState(false)
                        })
    }

    override fun isActive(url: String): Boolean = model.isActive(url)

    @SuppressLint("CheckResult")
    override fun changeFavouriteState(url: String) {
        Completable.fromAction { model.changeFavouriteState(url) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, Throwable::printStackTrace)
    }

}