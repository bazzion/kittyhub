package com.example.kittyhub.ui.favouriteList

interface IFavouriteListPresenter{
    fun start()
    fun isActive(url: String): Boolean
    fun changeFavouriteState(url: String)
    fun openFavouriteKitty()
}