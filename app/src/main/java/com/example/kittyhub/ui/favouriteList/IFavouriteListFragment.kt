package com.example.kittyhub.ui.favouriteList

interface IFavouriteListFragment{
    fun init()
    fun showError(message: String)
    fun loadDataInList(photoUrl: List<String>)

}