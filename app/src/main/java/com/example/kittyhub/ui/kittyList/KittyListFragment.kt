package com.example.kittyhub.ui.kittyList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.kittyhub.R
import com.example.kittyhub.features.AdapterKittyList
import com.example.kittyhub.features.CovertConfig
import com.example.kittyhub.ui.FragmentStack
import com.example.kittyhub.ui.MainActivity
import com.example.kittyhub.ui.favouriteList.FavouriteListFragment
import kotlinx.android.synthetic.main.fragment_kitty_list.*
import kotlinx.android.synthetic.main.fragment_kitty_list.view.*
import nz.co.trademe.covert.Covert
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class KittyListFragment : Fragment(), IKittyListFragment, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {


    private val fragmentStack: FragmentStack by lazy { activity as MainActivity }
    private val presenter: IKittyListPresenter by inject { parametersOf(this@KittyListFragment) }
    private val adapter: AdapterKittyList by inject()
    private lateinit var recyclerView: RecyclerView
    private val covertConfig: CovertConfig by inject()
    private lateinit var covert: Covert

    fun newInstance(): KittyListFragment {
        val regFragment = KittyListFragment()
        val args = Bundle()
        regFragment.arguments = args
        return regFragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_kitty_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefreshLayout.setOnRefreshListener(this)
        swipeRefreshLayout.isRefreshing = true
        view.favouriteActionButton.setOnClickListener(this)
        presenter.start()
    }

    override fun onClick(v: View?) {
        when (v) {
            v!!.favouriteActionButton -> {
                swipeRefreshLayout.isRefreshing = false
                fragmentStack.push(FavouriteListFragment().newInstance())
            }
        }
    }

    override fun onRefresh() {
        swipeRefreshLayout.isRefreshing = true
        presenter.loadKitty()
    }

    override fun init() {
        if(view == null) {

        } else {
            view!!.swipeRefreshLayout.isRefreshing = true
            recyclerView = view!!.findViewById(R.id.recyclerViewKitty)
            recyclerView.layoutManager = LinearLayoutManager(context)
            presenter.loadKitty()
        }
    }

    override fun showError(message: String) {
        recyclerView.adapter = adapter
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun loadDataInList(photoUrl: List<String>) {
        if (view != null) {
            swipeRefreshLayout.isRefreshing = false
            adapter.photoList = photoUrl.toMutableList()
            covert = Covert.with(covertConfig.create())
                    .setIsActiveCallback { viewHolder -> presenter.isActive(adapter.photoList[viewHolder.adapterPosition]) }
                    .doOnSwipe { viewHolder, _ -> presenter.changeFavouriteState(adapter.photoList[viewHolder.adapterPosition]) }
                    .attachTo(recyclerView)
            adapter.covert = covert
            recyclerView.adapter = adapter
        }
    }

    override fun setRefreshState(state: Boolean) {
            view?.swipeRefreshLayout?.isRefreshing = state
    }
}