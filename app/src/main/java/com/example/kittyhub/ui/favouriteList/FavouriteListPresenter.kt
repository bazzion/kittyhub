package com.example.kittyhub.ui.favouriteList

import android.annotation.SuppressLint
import com.example.kittyhub.useCase.IKittyDataUseCase
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FavouriteListPresenter(private val view: IFavouriteListFragment, private val model: IKittyDataUseCase) : IFavouriteListPresenter {
    override fun start() {
        view.init()
    }

    override fun isActive(url: String): Boolean = model.isActive(url)

    @SuppressLint("CheckResult")
    override fun changeFavouriteState(url: String) {
        Completable.fromAction { model.changeFavouriteState(url) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, Throwable::printStackTrace)
    }

    @SuppressLint("CheckResult")
    override fun openFavouriteKitty() {
        model.getAllFavourite()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(
                        {
                            view.loadDataInList(it)
                        },
                        { t ->
                            view.showError(t.localizedMessage)
                        })
    }
}