package com.example.kittyhub.ui.kittyList

interface IKittyListPresenter {
    fun start()
    fun loadKitty()
    fun isActive(url: String): Boolean
    fun changeFavouriteState(url: String)
}