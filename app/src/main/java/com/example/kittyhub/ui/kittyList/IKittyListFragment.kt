package com.example.kittyhub.ui.kittyList

interface IKittyListFragment {
    fun init()
    fun showError(message: String)
    fun loadDataInList(photoUrl: List<String>)
    fun setRefreshState(state: Boolean)
}