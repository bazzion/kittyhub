package com.example.kittyhub.ui

import androidx.fragment.app.Fragment

interface FragmentStack {
    fun push(fragment: Fragment)
    fun pop()
}