package com.example.kittyhub.repository.network

import com.example.kittyhub.data.network.ApiKittyHub
import com.example.kittyhub.data.network.NetworkClient
import com.example.kittyhub.data.network.dataModel.KittyModel
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class KittyNetworkRepository : KoinComponent {

    private val client: NetworkClient by inject()

    fun getPhotoUrl(): Single<List<String>> {
        return getKittyData()
            .map { getItems(it) }
            .subscribeOn(Schedulers.io())
    }

    private fun getItems(list: MutableList<KittyModel>): List<String> = list.map{it.url!!}


    private fun getKittyData(): Single<MutableList<KittyModel>> =
        client.getClient(ApiKittyHub::class.java, client.baseUrl).getDailyCurrency()
}