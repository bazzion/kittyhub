package com.example.kittyhub.repository.local

import android.annotation.SuppressLint
import com.example.kittyhub.data.db.FavouriteKittyDao
import com.example.kittyhub.data.db.FavouriteKittyData
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FavouriteKittyRepository(private val favouriteKittyDao: FavouriteKittyDao): IFavouriteKittyRepository {

    override fun insert(url: String) {
        val favouriteKittyData = FavouriteKittyData()
        favouriteKittyData.imageUrl = url
        favouriteKittyDao.insert(favouriteKittyData)

    }

    override fun isActive(url: String): Boolean = favouriteKittyDao.getByUrl(url) != null


    @SuppressLint("CheckResult")
    override fun getAllFavourite(): Observable<List<String>> {
        return  favouriteKittyDao.getAll()
                .map { convertList(it) }
                .subscribeOn(Schedulers.io())
    }

    private fun convertList(list: List<FavouriteKittyData>): List<String> = list.map {it.imageUrl}

    override fun delete(favouriteKittyData: FavouriteKittyData) {
        Completable.fromAction { favouriteKittyDao.delete(favouriteKittyData) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun deleteByUrl(url: String) {
        Completable.fromAction { favouriteKittyDao.deleteByUrl(url) }
            .observeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun changeFavouriteState(url: String) {
        if (favouriteKittyDao.getByUrl(url) == null) {
            insert(url)
        } else {
            deleteByUrl(url)
        }
    }
}