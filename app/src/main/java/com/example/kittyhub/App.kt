package com.example.kittyhub

import android.app.Application
import com.example.kittyhub.koin.favouriteModule
import com.example.kittyhub.koin.globalModule
import com.example.kittyhub.koin.kittyModule
import com.example.kittyhub.koin.roomModule
import org.koin.android.ext.android.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(globalModule, kittyModule, roomModule, favouriteModule))
    }
}