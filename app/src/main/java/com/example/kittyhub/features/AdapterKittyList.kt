package com.example.kittyhub.features

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.item_kitty.view.*
import nz.co.trademe.covert.Covert



class AdapterKittyList : RecyclerView.Adapter<AdapterKittyList.ViewHolder>() {


    var photoList: MutableList<String> = mutableListOf()
    lateinit var covert: Covert

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterKittyList.ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(com.example.kittyhub.R.layout.item_kitty, parent, false))

    override fun getItemCount(): Int = photoList.size

    override fun onBindViewHolder(holder: AdapterKittyList.ViewHolder, position: Int) {
        covert.drawCornerFlag(holder)
        holder.bind(photoList[position])
    }


    fun getUrlByPosition(position: Int): String = photoList[position]

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (!payloads.contains(Covert.SKIP_FULL_BIND_PAYLOAD)) {
            onBindViewHolder(holder, position)
        }
    }

    class ViewHolder(private val containerView: View) : RecyclerView.ViewHolder(containerView) {


        fun bind(photoUrl: String) {
            containerView.setOnLongClickListener {
                val downloadDialog = DownloadDialog().newInstance(photoUrl)
                val fragmentManager = (containerView.context as FragmentActivity).supportFragmentManager
                downloadDialog.show(fragmentManager, "download")
                return@setOnLongClickListener true
            }
            Glide.with(containerView)
                    .load(photoUrl)
                    .transition(
                            DrawableTransitionOptions.withCrossFade())
                    .optionalFitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(containerView.imageOfKitty)
        }
    }
}
