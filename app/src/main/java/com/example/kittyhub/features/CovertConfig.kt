package com.example.kittyhub.features

import nz.co.trademe.covert.Covert



class CovertConfig {

    fun create(): Covert.Config = Covert.Config(
            Covert.Icon(
                    com.example.kittyhub.R.drawable.ic_star_black_24dp,
                    com.example.kittyhub.R.color.black,
                    com.example.kittyhub.R.color.black),
            Covert.Icon(
                    com.example.kittyhub.R.drawable.ic_star_border_black_24dp,
                    com.example.kittyhub.R.color.black,
                    com.example.kittyhub.R.color.black
            ),
            com.example.kittyhub.R.color.yellow,
            com.example.kittyhub.R.color.yellow,
            true,
            Covert.CornerFlag.Round
    )
}