package com.example.kittyhub.features

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_download.view.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DownloadDialog: DialogFragment() {

    fun newInstance(url: String): DownloadDialog {
        val fragment = DownloadDialog()
        val bundle = Bundle()
        bundle.putString("image_url", url)
        fragment.arguments = bundle
        return fragment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(com.example.kittyhub.R.layout.dialog_download, container, false)
        val url = arguments!!.getString("image_url")
        view.downloadTextView.setOnClickListener {
            ImageDownloader(context!!.applicationContext, url).download()
            dialog!!.dismiss()
        }
        return view
    }
}