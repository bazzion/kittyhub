package com.example.kittyhub.features

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Environment
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import java.io.File
import java.io.FileOutputStream
import java.util.*


class ImageDownloader(private var context: Context, private var url: String) {


    @SuppressLint("CheckResult")
    fun download() {
        Glide.with(context).asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>(1024, 768) {
                override fun onLoadCleared(placeholder: Drawable?) {}

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) = saveImage(resource)
            })
    }

    private fun saveImage(resource: Bitmap?) {
        Thread(Runnable {
            val folder = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val imageFileName = UUID.randomUUID().toString() + ".png"
            val imageFile = File(folder, imageFileName)
            try {
                imageFile.createNewFile()
                val fOut = FileOutputStream(imageFile)
                resource!!.compress(Bitmap.CompressFormat.PNG, 100, fOut)
                fOut.close()
                Toast.makeText(context, "!!", Toast.LENGTH_LONG).show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }).start()
    }

}