package com.example.kittyhub.useCase.local

import com.example.kittyhub.data.db.FavouriteKittyData
import io.reactivex.Observable

interface ILocalFavouriteKittyUseCase {

    fun insert(url: String)

    fun delete(favouriteKittyData: FavouriteKittyData)

    fun getAllFavourite(): Observable<List<String>>

    fun isActive(url: String) : Boolean

    fun deleteByUrl(url: String)

    fun changeFavouriteState(url: String)
}