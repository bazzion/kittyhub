package com.example.kittyhub.useCase.remote

import io.reactivex.Single

interface IRemoteKittyUseCase {

    fun getList(): Single<List<String>>
}