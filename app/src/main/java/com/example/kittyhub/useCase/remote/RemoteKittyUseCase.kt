package com.example.kittyhub.useCase.remote

import com.example.kittyhub.repository.network.KittyNetworkRepository
import io.reactivex.Single
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class RemoteKittyUseCase: IRemoteKittyUseCase, KoinComponent {

    private val networkRepository: KittyNetworkRepository by inject()

    override fun getList(): Single<List<String>> = networkRepository.getPhotoUrl()

}