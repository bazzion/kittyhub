package com.example.kittyhub.useCase

import com.example.kittyhub.data.db.FavouriteKittyData
import com.example.kittyhub.useCase.local.ILocalFavouriteKittyUseCase
import com.example.kittyhub.useCase.remote.IRemoteKittyUseCase
import io.reactivex.Observable
import io.reactivex.Single
import org.koin.standalone.KoinComponent

class KittyDataUseCase(private val remoteData: IRemoteKittyUseCase, private val localData: ILocalFavouriteKittyUseCase) : IKittyDataUseCase, KoinComponent {


    override fun getRemoteData(): Single<List<String>> = remoteData.getList()

    override fun insert(url: String) = localData.insert(url)

    override fun delete(favouriteKittyData: FavouriteKittyData) = localData.delete(favouriteKittyData)

    override fun getAllFavourite(): Observable<List<String>> = localData.getAllFavourite()

    override fun isActive(url: String): Boolean = localData.isActive(url)

    override fun deleteByUrl(url: String) = localData.deleteByUrl(url)

    override fun changeFavouriteState(url: String) = localData.changeFavouriteState(url)
}