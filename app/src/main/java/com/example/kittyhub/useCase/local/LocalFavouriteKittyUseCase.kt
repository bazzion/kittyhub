package com.example.kittyhub.useCase.local

import com.example.kittyhub.data.db.FavouriteKittyData
import com.example.kittyhub.repository.local.IFavouriteKittyRepository
import io.reactivex.Observable

class LocalFavouriteKittyUseCase(private val iFavouriteKittyRepository: IFavouriteKittyRepository): ILocalFavouriteKittyUseCase {

    override fun insert(url: String) = iFavouriteKittyRepository.insert(url)

    override fun delete(favouriteKittyData: FavouriteKittyData) = iFavouriteKittyRepository.delete(favouriteKittyData)

    override fun getAllFavourite(): Observable<List<String>> = iFavouriteKittyRepository.getAllFavourite()

    override fun isActive(url: String): Boolean = iFavouriteKittyRepository.isActive(url)

    override fun deleteByUrl(url: String) = iFavouriteKittyRepository.deleteByUrl(url)

    override fun changeFavouriteState(url: String) = iFavouriteKittyRepository.changeFavouriteState(url)
}