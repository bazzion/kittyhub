package com.example.kittyhub.useCase

import com.example.kittyhub.data.db.FavouriteKittyData
import io.reactivex.Observable
import io.reactivex.Single

interface IKittyDataUseCase {

    fun getRemoteData(): Single<List<String>>

    fun insert(url: String)

    fun delete(favouriteKittyData: FavouriteKittyData)

    fun getAllFavourite(): Observable<List<String>>

    fun isActive(url: String) : Boolean

    fun deleteByUrl(url: String)

    fun changeFavouriteState(url: String)
}