package com.example.kittyhub.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import io.reactivex.Observable

@Dao
interface FavouriteKittyDao {

    @Query("SELECT * from favouriteKitty")
    fun getAll(): Observable<List<FavouriteKittyData>>

    @Query("SELECT image_url FROM favouriteKitty WHERE image_url= :url")
    fun getByUrl(url: String): String?

    @Query("DELETE FROM favouriteKitty WHERE image_url = :url")
    fun deleteByUrl(url: String)

    @Insert(onConflict = REPLACE)
    fun insert(favouriteKittyData: FavouriteKittyData)

    @Delete
    fun delete(favouriteKittyData: FavouriteKittyData)

}