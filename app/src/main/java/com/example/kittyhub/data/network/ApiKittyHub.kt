package com.example.kittyhub.data.network

import com.example.kittyhub.data.network.dataModel.KittyModel
import io.reactivex.Single
import retrofit2.http.GET

interface ApiKittyHub {

    @GET("/v1/images/search?limit=10")
//    @Headers("“x-api-key: 883ded2d-c055-4c72-b7c0-7bf713b6f9c2")
    fun getDailyCurrency(): Single<MutableList<KittyModel>>
}