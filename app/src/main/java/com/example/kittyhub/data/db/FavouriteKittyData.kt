package com.example.kittyhub.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "favouriteKitty")
data class FavouriteKittyData(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    @ColumnInfo(name = "image_url") var imageUrl: String
) {
    constructor() : this(null, "")
}