package com.example.kittyhub.data.network.dataModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class KittyModel(
    @SerializedName("breeds")
    @Expose
    var breeds: List<Any>? = null,
    @SerializedName("categories")
    @Expose
    var categories: List<Category>? = null,
    @SerializedName("id")
    @Expose
    var id: String? = null,
    @SerializedName("url")
    @Expose
    var url: String? = null
)
