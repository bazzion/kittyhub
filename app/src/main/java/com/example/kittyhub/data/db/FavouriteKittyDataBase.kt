package com.example.kittyhub.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [FavouriteKittyData::class], version = 1)
abstract class FavouriteKittyDataBase : RoomDatabase() {

    abstract fun favouriteKittyDao(): FavouriteKittyDao

}