package com.example.kittyhub.koin

import androidx.room.Room
import com.example.kittyhub.data.db.FavouriteKittyDataBase
import com.example.kittyhub.data.network.NetworkClient
import com.example.kittyhub.features.AdapterKittyList
import com.example.kittyhub.features.CovertConfig
import com.example.kittyhub.repository.local.FavouriteKittyRepository
import com.example.kittyhub.repository.local.IFavouriteKittyRepository
import com.example.kittyhub.repository.network.KittyNetworkRepository
import com.example.kittyhub.ui.favouriteList.FavouriteListPresenter
import com.example.kittyhub.ui.favouriteList.IFavouriteListFragment
import com.example.kittyhub.ui.favouriteList.IFavouriteListPresenter
import com.example.kittyhub.ui.kittyList.IKittyListFragment
import com.example.kittyhub.ui.kittyList.IKittyListPresenter
import com.example.kittyhub.ui.kittyList.KittyListPresenter
import com.example.kittyhub.useCase.IKittyDataUseCase
import com.example.kittyhub.useCase.KittyDataUseCase
import com.example.kittyhub.useCase.local.ILocalFavouriteKittyUseCase
import com.example.kittyhub.useCase.local.LocalFavouriteKittyUseCase
import com.example.kittyhub.useCase.remote.IRemoteKittyUseCase
import com.example.kittyhub.useCase.remote.RemoteKittyUseCase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

val globalModule = module {
    single { NetworkClient() }
    single { KittyNetworkRepository() }
    single { RemoteKittyUseCase() as IRemoteKittyUseCase }
    factory { AdapterKittyList() }
    single { CovertConfig() }
}

val kittyModule = module {
    single { KittyDataUseCase(get(), get()) as IKittyDataUseCase }
    factory { (fragment: IKittyListFragment) -> KittyListPresenter(fragment, get()) as IKittyListPresenter }
}

val favouriteModule = module {
    factory { (fragment: IFavouriteListFragment) -> FavouriteListPresenter(fragment, get()) as IFavouriteListPresenter }
}

val roomModule = module {
    single { Room.databaseBuilder(androidApplication(), FavouriteKittyDataBase::class.java, "favouriteKitty").allowMainThreadQueries().build() }
    single { get<FavouriteKittyDataBase>().favouriteKittyDao() }
    single { FavouriteKittyRepository(get()) as IFavouriteKittyRepository }
    single { LocalFavouriteKittyUseCase(get()) as ILocalFavouriteKittyUseCase }
}